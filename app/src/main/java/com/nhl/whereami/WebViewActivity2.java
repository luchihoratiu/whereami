package com.nhl.whereami;

import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class WebViewActivity2 extends FragmentActivity {
    WebView webview;
    private double longitude=0, latitude=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        Intent intent = getIntent();
        longitude = (Double) intent.getSerializableExtra("longitude");
        latitude = (Double) intent.getSerializableExtra("latitude");
        if(isNetworkAvailable())
        {
            webview = findViewById(R.id.webview);
            webview.setWebViewClient(new myWebClient());
            webview.getSettings().setJavaScriptEnabled(true);
            webview.getSettings().setLoadWithOverviewMode(true);
            webview.getSettings().setUseWideViewPort(true);

            webview.getSettings().setSupportZoom(true);
            webview.getSettings().setBuiltInZoomControls(true);
            webview.getSettings().setDisplayZoomControls(false);

            webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            webview.setScrollbarFadingEnabled(false);

            String city = "";

            try {
                List<Address> addresses;
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                if(!addresses.isEmpty()) {
                    String address = addresses.get(0).getAddressLine(0);
                    city = address.split(",")[1].replaceAll("[0-9]","").trim();
                }

            } catch (Exception e) {
                city = "";
            }

            if(isNetworkAvailable())
            {
                webview.loadUrl("https://en.wikipedia.org/wiki/" + city);
            }
        }
    }

    public boolean isNetworkAvailable()
    {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getApplicationContext().getSystemService(this.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public class myWebClient extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
