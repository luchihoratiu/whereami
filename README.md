Where am I? - Android App
====================

An app which let's you know your location and gives you info about said location.
You can see your surroundings on Google Maps, find out more about your location 
on WikiMapia and see what Wikipedia has to say about your current town location.
Currently supports only romanian language.

## Images

Here are some in app screenshots:

- - -
Location found:
- - -
![Alt text](https://bitbucket.org/luchihoratiu/whereami/raw/1cbeb3c475f96f056ebf079d06c30ff6b563ddab/screenshots/1.jpg)

- - -
Location not enabled:
- - -
![Alt text](https://bitbucket.org/luchihoratiu/whereami/raw/1cbeb3c475f96f056ebf079d06c30ff6b563ddab/screenshots/2.jpg)

- - -
Maps:
- - -
![Alt text](https://bitbucket.org/luchihoratiu/whereami/raw/1cbeb3c475f96f056ebf079d06c30ff6b563ddab/screenshots/3.jpg)

- - -
WikiMapia:
- - -
![Alt text](https://bitbucket.org/luchihoratiu/whereami/raw/1cbeb3c475f96f056ebf079d06c30ff6b563ddab/screenshots/4.jpg)

- - -
Wikipedia:
- - -
![Alt text](https://bitbucket.org/luchihoratiu/whereami/raw/1cbeb3c475f96f056ebf079d06c30ff6b563ddab/screenshots/5.jpg)

	
- - -